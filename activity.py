from abc import ABC, abstractclassmethod

class Person(ABC):

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department) -> None:
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        

    # getter

    def getFullName(self):
        fullname = f'{self.firstName} {self.lastName}'
        return fullname

    def addRequest(self):
        output = f'Request has been added'
        return output

    def checkRequest(self):
        pass

    def addUser(self):
        pass


    def get_firstName(self):
        output = f'First Name: {self.firstName}'
        return output
    
    def get_lastName(self):
        output = f'Last Name: {self.lastName}'
        return output
    
    def get_email(self):
        output = f'Email: {self.email}'
        return output

    def get_department(self):
        output = f'Department: {self.department}'
        return output

    # setter
    def set_firstName(self, firstName):
        self.firstName = firstName
      
      
    def set_lastName(self, lastName):
        self.lastName = lastName
          

    def set_email(self, email):
        self.email = email
       

    def set_department(self, department):
        self.department = department
        



    def login(self):
        output = f'{self.email} has logged in'
        return output

    def logout(self):
        output = f'{self.email} has logged out'
        return output

    
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department) -> None:
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        self.members = []

    # getter
    def getFullName(self):
        fullname = f'{self.firstName} {self.lastName}'
        return fullname

    def addRequest(self):
        output = f'Request has been added'
        return output

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def get_firstName(self):
        output = f'First Name: {self.firstName}'
        return output
    
    def get_lastName(self):
        output = f'Last Name: {self.lastName}'
        return output
    
    def get_email(self):
        output = f'Email: {self.email}'
        return output

    def get_department(self):
        output = f'Department: {self.department}'
        return output

    def get_members(self):
        output = self.members
        return output

    # setter
    def set_firstName(self, firstName):
        self.firstName = firstName
        output = f'{self.firstName} has been added'
        return output


    def set_lastName(self, lastName):
        self.lastName = lastName   
        output = f'{self.lastName} has been added' 
        return output  


    def set_email(self, email):
        self.email = email
        output = f'{self.email} has been added'
        return output


    def set_department(self, department):
        self.department = department
        output = f'{self.department} has been added'
        return output

    
    def addMember(self, member):
        self.members.append(member)
        return self.members


    def login(self):
        output = f'{self.email} has logged in'
        return output

    def logout(self):
        output = f'{self.email} has logged out'
        return output


class Admin(Person):
    def __init__(self, firstName, lastName, email, department) -> None:
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        

    # getter

    def getFullName(self):
        fullname = f'{self.firstName} {self.lastName}'
        return fullname

    def addRequest(self):
        output = f'Request has been added'
        return output

    def checkRequest(self):
        pass

    def addUser(self):
        output = f'User has been added'
        return output


    def get_firstName(self):
        output = f'First Name: {self.firstName}'
        return output
    
    def get_lastName(self):
        output = f'Last Name: {self.lastName}'
        return output
    
    def get_email(self):
        output = f'Email: {self.email}'
        return output

    def get_department(self):
        output = f'Department: {self.department}'
        return output

    # setter
    def set_firstName(self, firstName):
        self.firstName = firstName
      
      
    def set_lastName(self, lastName):
        self.lastName = lastName
          

    def set_email(self, email):
        self.email = email
       

    def set_department(self, department):
        self.department = department
        



    def login(self):
        output = f'{self.email} has logged in'
        return output

    def logout(self):
        output = f'{self.email} has logged out'
        return output

class Request:
    def __init__(self, name, requester, dateRequested) -> None:
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = ""

    # getter
    
    def get_name(self):
        output = f'Full Name: {self.name}'
        return output

    def get_requester(self):
        output = f'Requester: {self.requester}'
        return output

    def get_dateRequested(self):
        output = f'Date Requested: {self.dateRequested}'
        return output

    def get_status(self):
        output = f'Status: {self.status}'
        return output

    # setter

    def set_name(self, name):
        self.name = name
      
    def set_requester(self, requester):
        self.requester = requester
          

    def set_dateRequested(self, dateRequested):
        self.dateRequested = dateRequested

    def set_status(self, status):
        self.status = status
    
       
    def updateRequest(self):
        output = f'Request {self.name} has been {self.status}'
        return output
    

    def closeRequest(self):
        output = f'Request {self.name} has been {self.status}'
        return output
    
    def cancelRequest(self):
        output = f'Request {self.name} has been {self.status}'
        return output
    

        
    




# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")
employee1.getFullName()
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"
                    
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())

